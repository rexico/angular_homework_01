import {Component, EventEmitter, Input, Output} from '@angular/core';
import { Person } from "src/app/interfaces/person.interface";

@Component({
  selector: 'people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent {

  @Input()
    people!: Person[];
  @Output() btnDelPersClickEvent = new EventEmitter<number>();
  idx: number = -1;

  onDelPersBtnClick(idx: number) {
    this.idx = idx;
    console.log('Click from people # ' + this.idx);
    
    this.btnDelPersClickEvent.emit(this.idx);

  }
}
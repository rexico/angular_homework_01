import {Component, EventEmitter, Input, Output} from '@angular/core';
import { Person } from "src/app/interfaces/person.interface";

@Component({
  selector: 'person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent {

  @Input()
    person!: Person;
  @Output() btnDelClickEvent = new EventEmitter();

  onDelBtnClick() {
    console.log('Click from person');
    this.btnDelClickEvent.emit();

  }
}
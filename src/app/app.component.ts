import { Component, EventEmitter } from '@angular/core';
import { Person } from './interfaces/person.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'homework01';
  people : Person[] = [
    { firstName: 'firstName1', lastName: 'lastName1', age: 20}, 
    { firstName: 'firstName2', lastName: 'lastName2', age: 32}, 
    { firstName: 'firstName3', lastName: 'lastName3', age: 15}, 
    { firstName: 'firstName4', lastName: 'lastName4', age: 54}, 
    { firstName: 'firstName5', lastName: 'lastName5', age: 25}, 
    { firstName: 'firstName6', lastName: 'lastName6', age: 29}, 
    { firstName: 'firstName7', lastName: 'lastName7', age: 98}, 
    { firstName: 'firstName8', lastName: 'lastName8', age: 12}, 
    { firstName: 'firstName9', lastName: 'lastName9', age: 45}, 
    { firstName: 'firstName10', lastName: 'lastName10', age: 60}, 
    { firstName: 'firstName11', lastName: 'lastName11', age: 34}];
  

  delPers(idx: number) {
    console.log('Click from app # ' + idx);
    this.people.splice(idx, 1);
  }
}
